import { exec } from 'child_process';
import * as clc from 'cli-color';
import * as CLI from 'clui';
import * as fs from 'fs-extra';
import * as glob from 'glob';
import * as inquirer from 'inquirer';
import * as path from 'path';

interface Packages {
  [pkg: string]: string;
}

(async () => {
  const packagejson = require('../package.json');

  const Line = CLI.Line,
    LineBuffer = CLI.LineBuffer,
    Spinner = CLI.Spinner;

  const outputBuffer = new LineBuffer({
    x: 0,
    y: 0,
    width: 'console',
    height: 'console'
  });

  const saltoapisRE = RegExp(/^@saltoapis\/(\w+-)*v\d$/); // @saltoapis/*-vX

  clear();

  // show installed nebula packages
  new Line(outputBuffer)
    .column('Installed nebula packages', 100, [clc.green])
    .fill()
    .store();

  new Line(outputBuffer)
    .fill()
    .store();

  const packages = getInstalledPkgs();
  const anyInstalledPkgs = Object.keys(packages).length > 0;

  if (anyInstalledPkgs) {
    printPkgs(packages);
  } else {
    new Line(outputBuffer)
      .column('There are no installed @saltoapis packages', 100, [clc.red])
      .fill()
      .store();
  }

  new Line(outputBuffer)
    .fill()
    .store();
  new Line(outputBuffer)
    .fill()
    .store();
  new Line(outputBuffer)
    .fill()
    .store();

  new Line(outputBuffer)
    .column('Linked nebula packages', 100, [clc.green])
    .fill()
    .store();

  new Line(outputBuffer)
    .fill()
    .store();

  const linkedPkgs = getLinkedPkgs();
  const anyLinkedPackages = Object.keys(linkedPkgs).length > 0;
  if (anyLinkedPackages) {
    printPkgs(linkedPkgs);
  } else {
    new Line(outputBuffer)
      .column('There are no linked @saltoapis packages', 100, [clc.red])
      .fill()
      .store();
  }

  new Line(outputBuffer)
    .fill()
    .store();

  outputBuffer.output();

  const action = await askAction();
  const mergedPackages = Object.assign(packages, linkedPkgs);
  switch (action.action) {
    case 'install':
      install(mergedPackages, anyLinkedPackages);
      break;
    case 'link':
      link(mergedPackages);
      break;
    default:
      break;
  }

  function clear() {
    process.stdout.write('\x1B[2J'); // clear screen
  }

  function getLinkedPkgs(cwd = process.cwd()) {
    const modulesFolder = path.join(cwd, 'node_modules');
    const result = {};
    const linked =
      glob.sync('{@*/*,[^@]*}/', { cwd: modulesFolder })
        .map(match => match.slice(0, -1))
        .filter(file => {
          const stat = fs.lstatSync(path.join(modulesFolder, file));
          return stat.isSymbolicLink();
        });
    linked
      .forEach(pkg => {
        const version = fs.readJsonSync(path.join(modulesFolder, pkg, 'package.json')).version;
        if (saltoapisRE.test(pkg)) {
          result[pkg] = version;
        }
        return Object.assign(result, getLinkedPkgs(path.join(modulesFolder, pkg)));
      });
    return result;
  }

  function getInstalledPkgs(): Packages {
    const pkgs = {};
    for (const [pkg, version] of Object.entries(packagejson.dependencies)) {
      if (saltoapisRE.test(pkg)) {
        pkgs[pkg] = version;
      }
    }
    return pkgs;
  }

  function install(pkgs: Packages, anyLinkedPkgs: boolean) {
    const question = [
      {
        type: 'input',
        name: 'value',
        message: 'Which version would you like to install?',
      }
    ];

    inquirer.prompt(question).then(version => {
      const packagesToInstall = Object.keys(pkgs).join(`@${version.value} `) + `@${version.value}`;

      if (anyLinkedPkgs) {
        const spinner = new Spinner('Unlinking packages before installing, please wait...');
        spinner.start();

        exec(`npm unlink ${packagesToInstall}`, (err) => {
          if (err) {
            spinner.stop();
            console.log(err);
          } else {
            spinner.message('Installing packages...');
            exec(`npm i ${packagesToInstall}`, (err2) => {
              spinner.stop();
              if (err2) {
                console.log(err2);
              } else {
                console.log('Installation completed.');
              }
            });
          }
        });

      } else {
        const spinner = new Spinner('Installing, please wait...');
        spinner.start();

        exec(`npm i ${packagesToInstall}`, (err) => {
          spinner.stop();
          if (err) {
            console.log(err);
          } else {
            console.log('Installation completed.');
          }
        });
      }
    });
  }

  function link(pkgs: Packages) {
    const packagesToLink = Object.keys(pkgs).join(' ');

    const spinner = new Spinner('Uninstalling packages before linking, please wait...');
    spinner.start();
    exec(`npm uninstall ${packagesToLink}`, (err) => {
      if (err) {
        spinner.stop();
        console.log(err);
      } else {
        spinner.message('Linking packages, please wait ...');
        exec(`npm link ${packagesToLink}`, (err2) => {
          spinner.stop();
          if (err2) {
            console.log(err2);
          } else {
            console.log('Linking completed.');
          }
        });
      }
    });
  }

  function printPkgs(pkgs: Packages) {
    new Line(outputBuffer)
      .column('Package', 40, [clc.cyan])
      .column('Version', 60, [clc.cyan])
      .fill()
      .store();

    for (const pkg in pkgs) {
      if (pkgs.hasOwnProperty(pkg)) {
        new Line(outputBuffer)
          .column(pkg, 40)
          .column(pkgs[pkg], 60)
          .fill()
          .store();
      }
    }
  }

  async function askAction() {
    const actionList = [
      {
        value: 'install',
        name: 'Install/upgrade @saltoapis packages (unlinking previous packages)'
      },
      {
        value: 'link',
        name: 'Link @saltoapis packages (uninstalling previous packages)'
      },
      {
        value: 'exit',
        name: 'Exit'
      }
    ];
    const question = [
      {
        type: 'list',
        name: 'action',
        message: 'What would you like to do?',
        choices: actionList,
      }
    ];
    return inquirer.prompt(question);
  }
})();
