#!/usr/bin/env bash

set -u
set -e

mkdir -p ${REPORTS_DIR}
JEST_JUNIT_OUTPUT="${REPORTS_DIR}/junit.xml" npm run test:ci    
