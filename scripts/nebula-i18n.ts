import { exec } from 'child_process';
import * as fs from 'fs';
import * as http from 'http';
import * as path from 'path';
import { nebulaDateTrim } from '../src/app/shared/nebula-date/nebula-date.class';

const i18nLanguageDir = path.join(__dirname, '../src/i18n');
const i18nCountryDir = path.join(i18nLanguageDir, 'countries');
const i18nDateDir = path.join(i18nLanguageDir, 'dates');
const unicodeCldrCountryURL = 'https://raw.githubusercontent.com/unicode-cldr/cldr-localenames-full/master/main/';
const unicodeCldrDateURL = 'https://raw.githubusercontent.com/unicode-cldr/cldr-dates-full/master/main/';
const unicodeCldrCountryExt = 'territories.json';
const unicodeCldrDateExt = 'ca-gregorian.json';

let countryISOCodeList: Array<string> = [];

http.request({
  host: 'restcountries.eu',
  path: '/rest/v2/all?fields=alpha2Code'
}, (res) => {
  res.on('data', (content) => {
    countryISOCodeList = JSON.parse(content).map((country: any) => country.alpha2Code);
    fs.readdir(i18nLanguageDir, onReaddir);
  });
}).end();

const onReaddir = (readError: Error, files: string[]) => {
  if (readError) {
    console.error(readError);
    process.exit(1);
  }

  files.filter((file) => path.extname(file) === '.json').forEach((file) => {
    const language = file.replace('translation.', '').replace('.json', '');
    console.log(path.join(unicodeCldrCountryURL, language, unicodeCldrCountryExt));

    exec(`curl ${path.join(unicodeCldrCountryURL, language, unicodeCldrCountryExt)}`, (err: Error, stdout: string, stderr: string) => {
      console.log(stderr);
      if (err) {
        console.error(err);
        process.exit(1);
      }

      const data = JSON.parse(stdout).main[language].localeDisplayNames.territories;
      const result = {};
      Object.keys(data).forEach((key: any) => {
        if (countryISOCodeList.includes(key)) {
          result[key] = data[key];
        }
      });
      fs.writeFileSync(path.join(i18nCountryDir, `${language}.json`), JSON.stringify(result, null, 2) + '\n', 'utf8');
    });

    exec(`curl ${path.join(unicodeCldrDateURL, language, unicodeCldrDateExt)}`, (err: Error, stdout: string, stderr: string) => {
      console.log(stderr);
      if (err) {
        console.error(err);
        process.exit(1);
      }

      const result = nebulaDateTrim(JSON.parse(stdout).main[language].dates.calendars.gregorian);
      fs.writeFileSync(path.join(i18nDateDir, `${language}.json`), JSON.stringify(result, null, 2) + '\n', 'utf8');
    });
  });
};
