#!/usr/bin/env bash

set -u
set -e

n=0
until [ $n -ge 3 ]; do
  prod_vuln_qt=$(npm audit --json | jq '[.advisories | .[] | .findings | .[] | select(."dev" == false)] | length') && break
  n=$((n+1))
done

if [ "$prod_vuln_qt" -gt 0 ]; then
  echo "Some vulnerabilities were found in the production npm packages. Please run 'npm audit' to get more details."
  exit 1
fi
