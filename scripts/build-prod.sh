#!/usr/bin/env bash

set -u
set -e

ng build --prod --extractLicenses=false

for i in $(
	find dist -type f \
		-not -path 'dist/.well-known/*' \
		-not -path 'dist/ngsw-worker.js' \
		-not -path 'dist/safety-worker.js' \
		-not -path 'dist/worker-basic.min.js' \
		-not -path 'dist/ngsw.json'
); do
	FILE=$(echo "$i" | sed 's/^dist//g')
	if ! grep -q "${FILE}" "dist/ngsw.json"; then
		echo "${FILE} not included in dist/ngsw.json"
		echo "see https://gitlab.rnd.saltosystems.com/cloud-architecture/nebula-web/issues/142"
		exit 1
	fi
done

if ! ls dist/theme-0.* 1> /dev/null 2>&1; then
	echo "The themes are generating with anonymous chunk names"
	exit 1
fi

if ls dist/main-es5.* 1> /dev/null 2>&1; then
	echo "Unnecesary ES5 bundle detected"
	echo "see https://gitlab.rnd.saltosystems.com/cloud-architecture/nebula-web/issues/143"
	exit 1
fi

find src/assets -type f -not -name ".*" -print0 | while read -d $'\0' file; do
	file_to_check=${file/src\/assets/dist}
	echo "testing that ${file_to_check} is in the bundle"
	test -f ${file_to_check%.*}.*.${file##*.}
done
