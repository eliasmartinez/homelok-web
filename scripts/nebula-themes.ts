import * as fs from 'fs-extra';
import * as glob from 'glob';
import * as path from 'path';

// info taken from https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
const mimeType = {
  '.apng': 'image/apng',
  '.bmp': 'image/bmp',
  '.gif': 'image/gif',
  '.ico': 'image/x-icon',
  '.cur': 'image/x-icon',
  '.jpg': 'image/jpeg',
  '.jpeg': 'image/jpeg',
  '.jfif': 'image/jpeg',
  '.pjpeg': 'image/jpeg',
  '.pjp': 'image/jpeg',
  '.png': 'image/png',
  '.svg': 'image/svg+xml'
};

// TODO --> Replace source folder
const source = 'src/homelok-icons';
const output = 'src/generated/homelok-icons';

fs.removeSync(output);
fs.mkdirSync(output);
fs.mkdirSync(path.join(output, 'pwa-icons'));

const onerror = (err: Error) => {
  if (err) {
    console.error(err);
    process.exit(1);
  }
};

fs.readdirSync(source).forEach((folder: string) => {
  const input = path.join(source, folder);
  if (fs.statSync(input)?.isDirectory()) {
    const config = JSON.parse(fs.readFileSync(path.join(input, 'theme.json'), 'utf8'));
    const logo = glob.sync(path.join(input, 'logo.*'))[0];
    config.logo = `data:${mimeType[path.extname(logo)]};base64,${fs.readFileSync(logo, { encoding: 'base64' })}`;
    const favicon = glob.sync(path.join(input, 'favicon.*'))[0];
    config.favicon = `data:${mimeType[path.extname(favicon)]};base64,${fs.readFileSync(favicon, { encoding: 'base64' })}`;
    fs.writeFile(path.join(output, `${folder}.json`), JSON.stringify(config, null, 2) + '\n', 'utf8', onerror);
    fs.copy(path.join(input, 'icons'), path.join(output, 'pwa-icons', folder), onerror);
  }
});
