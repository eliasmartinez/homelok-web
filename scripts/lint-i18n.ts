import * as fs from 'fs';
import * as path from 'path';

const i18nFolder = `${__dirname}/../src/i18n`;
const i18nSubfolders = ['dates', 'countries'];

fs.readdirSync(i18nFolder)
  .filter(filename => path.extname(filename) === '.json')
  .map(filename => filename.replace('translation.', ''))
  .forEach(filename => {
    i18nSubfolders.forEach(subfolder => {
      if (!fs.existsSync(`${i18nFolder}/${subfolder}/${filename}`)) {
        console.error(`${i18nFolder}/${subfolder}/${filename} doesn't exist`);
        process.exit(1);
      }
    });
  });
