if (process.argv[2] !== 'push' && process.argv[2] !== 'pull') {
  console.log('Should pass either "push" or "pull" as argument.');
  process.exit(1);
}

const exec = require('child_process').execSync;
const i18nRepo = __dirname + '/../i18n-repo';
const i18nFront = __dirname + '/../src/i18n';
const enLangFile = 'translation.en.json';

exec('rm -rf ' + i18nRepo);
exec('git clone https://weblate-scm.saltowebservices.org/SALTO-Systems/nebula-frontend.git i18n-repo', { stdio: 'ignore' });

// download the translations
if (process.argv[2] === 'pull') {
  exec('rm ' + i18nRepo + '/' + enLangFile);
  exec('cp ' + i18nRepo + '/translation.*.json ' + i18nFront);
  exec('rm -rf ' + i18nRepo);
  console.log('Done!');
}

// publish the English language file
if (process.argv[2] === 'push') {
  const source = require(i18nFront + '/' + enLangFile);
  const dest = require(i18nRepo + '/' + enLangFile);

  const keysToAdd = Object.keys(source).filter(x => !dest[x]);
  if (keysToAdd.length) {
    console.log('\n%s \x1b[32m\033[1m%s\x1b[0m:', 'Strings to', 'ADD');
    keysToAdd.forEach(key => console.log('    ' + key + ' ("' + source[key] + '")'));
  }

  const keysToUpdate = Object.keys(source).filter(x => source[x] !== dest[x] && dest[x]);
  if (keysToUpdate.length) {
    console.log('\n%s \x1b[33m\033[1m%s\x1b[0m:', 'Strings to', 'UPDATE');
    keysToUpdate.forEach(key => console.log('    ' + key + ' ("' + dest[key] + '" => "' + source[key] + '")'));
  }

  const keysToDelete = Object.keys(dest).filter(x => !source[x]);
  if (keysToDelete.length) {
    console.log('\n%s \x1b[31m\033[1m%s\x1b[0m:', 'Strings to', 'DELETE');
    keysToDelete.forEach(key => console.log('    ' + key + ' ("' + dest[key] + '")'));
  }

  if (!keysToAdd.length && !keysToUpdate.length && !keysToDelete.length) {
    console.log('No changes to sync');
    exec('rm -rf ' + i18nRepo);
    process.exit();
  }

  process.stdout.write('\nContinue? (Y/n): ');
  process.stdin.addListener('data', d => {
    process.stdin.pause();
    const val = d.toString().trim().toLowerCase();
    if (val === '' || val === 'y') {
      pushChanges();
    }
    exec('rm -rf ' + i18nRepo);
  });

  function pushChanges() {
    exec('cp ' + i18nFront + '/' + enLangFile + ' ' + i18nRepo + '/' + enLangFile);
    exec('cd ' + i18nRepo + ' && git commit -am "Update English strings" && git push origin master');
    console.log('Done!');
  }
}
