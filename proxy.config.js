module.exports = {
    '*': {
        bypass(req) {
            if (req.baseUrl + req.path === '/oauth2redirect') {
                return '/html/oauth2redirect.html';
            }
        },
    },
};
