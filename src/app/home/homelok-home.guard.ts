import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import {HomelokAuthnService} from '../shared/homelok-authnn/homelok-authn.service';
import {HomelokInstallationService} from '../shared/homelok-authnn/homelok-installation.service';

@Injectable({
  providedIn: 'root'
})
export class HomelokHomeGuard implements CanActivate {

  constructor(
    private router: Router,
    private homelokAuthnService: HomelokAuthnService,
    private homelokInstallationService: HomelokInstallationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> {
    const isAuthenticated = this.homelokAuthnService.isAuthenticated();
    return of(isAuthenticated);
  }
}
