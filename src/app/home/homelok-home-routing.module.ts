import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomelokHomeGuard} from './homelok-home.guard';
import {HomelokHomeComponent} from './homelok-home.component';

const homeRoutes: Routes = [{
  path: '',
  runGuardsAndResolvers: 'paramsOrQueryParamsChange',
  canActivate: [HomelokHomeGuard],
  component: HomelokHomeComponent,
}
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule],
  providers: []
})

export class HomelokHomeRoutingModule { }
