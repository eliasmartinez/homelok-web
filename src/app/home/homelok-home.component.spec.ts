import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomelokHomeComponent } from './homelok-home.component';

describe('HomelokHomeComponent', () => {
  let component: HomelokHomeComponent;
  let fixture: ComponentFixture<HomelokHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomelokHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomelokHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
