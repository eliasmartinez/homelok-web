import {NgModule} from '@angular/core';
import {HomelokHomeComponent} from './homelok-home.component';
import {HomelokHomeRoutingModule} from './homelok-home-routing.module';
import {HomelokTableModule} from '../shared/homelok-table/homelok-table.module';
import {AccessPointService, SaltoapisNebulaAccesspointV1Module} from '@saltoapis/nebula-accesspoint-v1';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {TranslateModule} from '@ngx-translate/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {SaltoapisNebulaCalendarV1Module} from '@saltoapis/nebula-calendar-v1';
import {SaltoapisNebulaDeviceV1Module} from '@saltoapis/nebula-device-v1';
import {MatSortModule} from '@angular/material/sort';
import {MatMenuModule} from '@angular/material/menu';
import {CommonModule} from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import {HomelokIconsModule} from '../shared/homelok-icons/homelok-icons.module';

@NgModule({
    declarations: [
        HomelokHomeComponent,
    ],
    imports: [
        CommonModule,
        HomelokHomeRoutingModule,
        HomelokTableModule,
        MatCardModule,
        MatTableModule,
        MatPaginatorModule,
        TranslateModule,
        FlexLayoutModule,
        MatCardModule,
        MatButtonModule,
        MatRadioModule,
        MatSelectModule,
        TranslateModule,
        SaltoapisNebulaAccesspointV1Module,
        SaltoapisNebulaCalendarV1Module,
        SaltoapisNebulaDeviceV1Module,
        MatSortModule,
        MatIconModule,
        HomelokIconsModule,
    ],
    providers: [
        AccessPointService,
    ],
    exports: [
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatMenuModule,
    ]
})

export class HomelokHomeModule { }
