import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomelokAuthGuard} from './shared/homelok-authnn/homelok-authn.guard';

const routes: Routes = [
  {
    path: 'home',
    canActivate: [HomelokAuthGuard],
    loadChildren: () => import('./home/homelok-home.module').then(m => m.HomelokHomeModule)
  },
  {
    path: 'access-points',
    canActivate: [HomelokAuthGuard],
    loadChildren: () => import('./access-points/access-points.module').then(m => m.AccessPointsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    onSameUrlNavigation: 'reload'
  })],
  exports: [RouterModule]
})
export class HomelokRoutingModule { }
