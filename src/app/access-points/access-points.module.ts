import {NgModule} from '@angular/core';
import {AccessPointListComponent} from './access-point-list/access-point-list.component';
import {CommonModule} from '@angular/common';
import {HomelokTableModule} from '../shared/homelok-table/homelok-table.module';
import {AccessPointsRoutingModule} from './access-points-routing.module';
import {AccessPointService} from '@saltoapis/nebula-accesspoint-v1';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {UserService} from '@saltoapis/nebula-user-v1';

@NgModule({
    declarations: [
        AccessPointListComponent,
    ],
    imports: [
        CommonModule,
        AccessPointsRoutingModule,
        HomelokTableModule,
        MatProgressSpinnerModule
    ],
    providers: [
        AccessPointService,
        UserService
    ]
})

export class AccessPointsModule { }
