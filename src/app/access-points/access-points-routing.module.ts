import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessPointListComponent } from './access-point-list/access-point-list.component';
import { AccessPointListResolver } from './access-point-list/access-point-list.resolver';
import {HOMELOK_PERMISSIONS} from '../shared/homelok-authz/homelok-authz.service';
import {UserListResolver} from './access-point-list/userrrrs/user-list.resolver';

export const accessPointRoutes: Routes = [
    {
        path: '',
        runGuardsAndResolvers: 'paramsOrQueryParamsChange',
        component: AccessPointListComponent,
        resolve: {
            dataSource: UserListResolver
        },
        data: { permissions: [HOMELOK_PERMISSIONS.ACCESSPOINT_LIST] }
    },
];

@NgModule({
    imports: [RouterModule.forChild(accessPointRoutes)],
    exports: [RouterModule],
    providers: [AccessPointListResolver, UserListResolver]
})
export class AccessPointsRoutingModule { }
