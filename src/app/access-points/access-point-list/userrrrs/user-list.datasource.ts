import {ActivatedRouteSnapshot, Router} from '@angular/router';
import {ListUsersRequest, ListUsersResponse, User, UserService} from '@saltoapis/nebula-user-v1';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {GetPageFunctionResponse, HomelokListDataSource} from '../../../shared/homelok-table/homelok-list-data-source';
import {HomelokInstallationService} from '../../../shared/homelok-authnn/homelok-installation.service';

export class UserListDataSource extends HomelokListDataSource<User> {

  filterableFields = ['display_name'];

  constructor(
    protected router: Router,
    protected route: ActivatedRouteSnapshot,
    private userService: UserService,
    private nebulaInstallationService: HomelokInstallationService,
    protected orderBy: string
  ) {
    super();
  }

  protected loadPageFunction(
    filterCriteria: string,
    pageSize: number,
    pageToken: string,
    orderBy: string
  ): Observable<GetPageFunctionResponse<User>> {
    const listUsersRequest = new ListUsersRequest();
    listUsersRequest.setParent(this.nebulaInstallationService.getCurrentInstallationName());
    listUsersRequest.setFilter(filterCriteria);
    listUsersRequest.setPageToken(pageToken);
    listUsersRequest.setPageSize(pageSize);
    listUsersRequest.setOrderBy(orderBy);
    return this.userService.listUsers(listUsersRequest)
      .pipe(
        map((response: ListUsersResponse) => {
          return {
            pageElements: response.getUsersList(),
            nextPageToken: response.getNextPageToken()
          };
        })
      );
  }
}
