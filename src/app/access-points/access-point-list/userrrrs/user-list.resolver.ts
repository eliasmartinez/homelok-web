import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { UserService } from '@saltoapis/nebula-user-v1';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserListDataSource } from './user-list.datasource';
import {HomelokInstallationService} from '../../../shared/homelok-authnn/homelok-installation.service';

@Injectable()
export class UserListResolver implements Resolve<UserListDataSource> {
  constructor(
    private router: Router,
    private userService: UserService,
    private nebulaInstallationService: HomelokInstallationService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<UserListDataSource> {
    const dataSource = new UserListDataSource(this.router, route, this.userService, this.nebulaInstallationService, 'display_name asc');
    return dataSource.firstPage(dataSource.getFilterCriteria(route.queryParams), route.queryParams.order)
      .pipe(
        map(() => dataSource)
      );
  }

}
