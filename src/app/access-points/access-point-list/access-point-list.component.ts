import {Component} from '@angular/core';
import {AccessPointListDataSource} from './access-point-list-data-source';

@Component({
  selector: 'access-point-list',
  templateUrl: './access-point-list.component.html',
  styleUrls: ['./access-point-list.component.scss']
})
export class AccessPointListComponent {

  displayedColumns = ['display_name'];
  dataSource: AccessPointListDataSource;
  title = 'Access Points';
}
