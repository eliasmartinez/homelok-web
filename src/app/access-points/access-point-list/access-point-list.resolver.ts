import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { AccessPointService } from '@saltoapis/nebula-accesspoint-v1';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {HomelokInstallationService} from '../../shared/homelok-authnn/homelok-installation.service';
import {AccessPointListDataSource} from './access-point-list-data-source';

@Injectable()
export class AccessPointListResolver implements Resolve<AccessPointListDataSource> {

  constructor(
    private router: Router,
    private accessPointsService: AccessPointService,
    private homelokInstallationService: HomelokInstallationService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<AccessPointListDataSource> {
    const dataSource = new AccessPointListDataSource(this.router, route, this.accessPointsService, this.homelokInstallationService, 'display_name asc');
    return dataSource.firstPage(dataSource.getFilterCriteria(route.queryParams), route.queryParams.order)
      .pipe(
        map(() => dataSource)
      );
  }
}
