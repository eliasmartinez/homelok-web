import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { AccessPoint, AccessPointService, ListAccessPointsRequest, ListAccessPointsResponse } from '@saltoapis/nebula-accesspoint-v1';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {GetPageFunctionResponse, HomelokListDataSource} from '../../shared/homelok-table/homelok-list-data-source';
import {HomelokInstallationService} from '../../shared/homelok-authnn/homelok-installation.service';

export class AccessPointListDataSource extends HomelokListDataSource<AccessPoint> {
    public data: AccessPoint[];
    public filterableFields = ['display_name'];

    constructor(
        protected router: Router,
        protected route: ActivatedRouteSnapshot,
        private accessPointsService: AccessPointService,
        private homelokInstallationService: HomelokInstallationService,
        protected orderBy: string,
    ) {
        super();
    }

    protected loadPageFunction(
        filterCriteria: string,
        pageSize: number,
        pageToken: string,
        orderBy: string
    ): Observable<GetPageFunctionResponse<AccessPoint>> {
        const listAccessPointsRequest = new ListAccessPointsRequest();
        listAccessPointsRequest.setFilter(filterCriteria);
        listAccessPointsRequest.setParent(this.homelokInstallationService.getCurrentInstallationName());
        listAccessPointsRequest.setPageToken(pageToken);
        listAccessPointsRequest.setPageSize(pageSize);
        listAccessPointsRequest.setOrderBy(orderBy);
        return this.accessPointsService.listAccessPoints(listAccessPointsRequest)
            .pipe(
                map((response: ListAccessPointsResponse) => {
                    return {
                        pageElements: response.getAccessPointsList(),
                        nextPageToken: response.getNextPageToken()
                    };
                })
            );
    }
}
