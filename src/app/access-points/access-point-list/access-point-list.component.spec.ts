import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessPointListComponent } from './access-point-list.component';

describe('AccessPointListComponent', () => {
  let component: AccessPointListComponent;
  let fixture: ComponentFixture<AccessPointListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessPointListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessPointListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
