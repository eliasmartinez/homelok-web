import {NgModule} from '@angular/core';

import {HomelokComponent} from './homelok.component';
import {HomelokAuthnModule} from './shared/homelok-authnn/homelok-authn.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HomelokTranslateLoader} from './shared/homelok-i18n/homelok-translate-loader';
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/homelok-environment';
import {HttpClientModule} from '@angular/common/http';
import {HomelokRoutingModule} from './homelok-routing.module';
import {GrpcUrlResolver} from '@saltoapis/grpc-web-client';
import {SaltoApiUrlResolver} from './shared/grpc/salto-api-url.resolver';
import {HomelokTableModule} from './shared/homelok-table/homelok-table.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material/icon';
import {HomelokNavbarComponent} from './shared/homelok-navbar/homelok-navbar.component';
import {HomelokSidebarComponent} from './shared/homelok-sidebar/homelok-sidebar.component';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {HomelokIconsModule} from './shared/homelok-icons/homelok-icons.module';
import {DatePipe} from '@angular/common';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    HomelokComponent,
    HomelokNavbarComponent,
    HomelokSidebarComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    HttpClientModule,
    HomelokAuthnModule,
    HomelokRoutingModule,
    HomelokTableModule,
    HomelokIconsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.enableSW}),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: () => new HomelokTranslateLoader()
      }
    }),
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatProgressSpinnerModule
  ],
  providers: [
    DatePipe,
    {
      provide: GrpcUrlResolver,
      useClass: SaltoApiUrlResolver
    },
  ],
  bootstrap: [HomelokComponent]
})
export class HomelokModule { }
