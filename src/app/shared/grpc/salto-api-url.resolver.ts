import { Injectable } from '@angular/core';
import { GrpcUrlResolver } from '@saltoapis/grpc-web-client';
import { environment } from '../../../environments/homelok-environment';
import {HomelokEnvironment} from '../../../environments/environment.model';

@Injectable()
export class SaltoApiUrlResolver extends GrpcUrlResolver {

  private baseUrl = 'https://api.saltonebula.com';
  private environment: HomelokEnvironment;

  constructor() {
    super();
    this.environment = environment;
  }

  getBaseUrl(): string {
    return this.baseUrl;
  }
}
