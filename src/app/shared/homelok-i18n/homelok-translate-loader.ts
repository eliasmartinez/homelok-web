import {TranslateLoader} from '@ngx-translate/core';
import {from, Observable} from 'rxjs';

export class HomelokTranslateLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    return from(import(`../../../i18n/translation.${lang}.json`).then(res => res.default));
  }
}
