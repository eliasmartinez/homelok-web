import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {HomelokListDataSource} from './homelok-list-data-source';
import {SelectionModel} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'homelok-table',
  templateUrl: './homelok-table.component.html',
  styleUrls: ['./homelok-table.component.scss']
})
export class HomelokTableComponent<T> implements OnInit {

  @Input() dataSource: HomelokListDataSource<T>;
  @Input() displayedColumns: string [];
  @Input() title: string;
  @Input() checkbox: boolean;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  selectedRow: ElementRef;

  selection: SelectionModel<T>;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.data.subscribe((data: { dataSource: HomelokListDataSource<T> }) => {
      this.dataSource = data.dataSource;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.selection = new SelectionModel<T>(true, []);
    });

    // Adding checkbox column
    if ( this.checkbox ) {
      this.displayedColumns = ['checkbox', ...this.displayedColumns];
    }
  }

  isAllSelected(): boolean {
    const numSelect = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelect === numRows;
  }

  masterToggle(): void {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: T): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${String(row)}`;
  }

}
