import { AfterViewInit, Directive, ElementRef, Host, Input, OnDestroy, Optional, Renderer2 } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import {HomelokListDataSource} from './homelok-list-data-source';

@Directive({
  /* tslint:disable-next-line:directive-selector */
  selector: 'mat-table'
})
export class HomelokEmptyListDirective implements AfterViewInit, OnDestroy {

  private _dataSource: HomelokListDataSource<any>;
  private _subscription: Subscription;
  private _emptyList = true;

  private _elementsToHide = ['mat-table', 'mat-paginator', '.header-actions'];
  private _mainElement: HTMLElement;

  @Input() homelokEmptyTitle: string;
  @Input() homelokEmptyMessage: string;

  constructor(
    @Host() @Optional() private table: MatTable<any>,
    private _element: ElementRef,
    private _renderer: Renderer2,
    private translate: TranslateService
  ) {
  }

  ngAfterViewInit() {
    if (this.table && this.table.dataSource) {
      this._dataSource = this.table.dataSource as HomelokListDataSource<any>;
      if (this.homelokEmptyTitle && this.homelokEmptyMessage) {
        this._createHTMLMessage();
        this._subscription = this._dataSource.connect(null).subscribe((data: any[]) => {
          this._emptyList = !data || data.length === 0;
          this._showEmptyMessage();
        });
      }
    }
  }

  private _showEmptyMessage() {
    const container = this._element.nativeElement.parentElement.parentElement;

    if (this._emptyList) {
      this._elementsToHide.forEach((el) => {
        const htmlEl: HTMLElement = container.querySelector(el);
        if (htmlEl) {
          htmlEl.style.display = 'none';
        }
      });
      const button = container.querySelector('.header-actions button');
      if (button) {
        this._renderer.appendChild(this._mainElement, button);
      }
      this._renderer.appendChild(this._element.nativeElement.parentElement, this._mainElement);
    } else {
      this._elementsToHide.forEach((el) => {
        const htmlEl: HTMLElement = container.querySelector(el);
        if (htmlEl) {
          htmlEl.style.display = '';
        }
      });
      const button = this._mainElement.querySelector('button');
      const header = container.querySelector('.header-actions');
      if (button && header) {
        this._renderer.appendChild(header, button);
      }
      this._renderer.removeChild(this._element.nativeElement.parentElement, this._mainElement);
    }
  }

  // TODO --> Revisar
  private _createHTMLMessage(): void {
    this._mainElement = document.createElement('div');
    this._renderer.setAttribute(this._mainElement, 'class', 'nebula-empty-list');
    const title = document.createElement('h1');
    title.innerHTML = this.translate.instant(this.homelokEmptyTitle);
    const text = document.createElement('h2');
    text.innerHTML = this.translate.instant(this.homelokEmptyMessage);
    this._renderer.appendChild(this._mainElement, title);
    this._renderer.appendChild(this._mainElement, text);

  }

  ngOnDestroy() {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }
}
