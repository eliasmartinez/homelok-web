import {NgModule} from '@angular/core';

import {CommonModule} from '@angular/common';
import {HomelokEmptyListDirective} from './homelok-empty-list.directive';
import {HomelokTableComponent} from './homelok-table.component';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatPaginatorModule} from '@angular/material/paginator';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    TranslateModule,
  ],
  declarations: [
    HomelokEmptyListDirective,
    HomelokTableComponent
  ],
  exports: [
    HomelokTableComponent
  ],
  providers: [],
})
export class HomelokTableModule { }
