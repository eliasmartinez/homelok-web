import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomelokTableComponent } from './homelok-table.component';

describe('HomelokTableComponent', () => {
  let component: HomelokTableComponent;
  let fixture: ComponentFixture<HomelokTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomelokTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomelokTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
