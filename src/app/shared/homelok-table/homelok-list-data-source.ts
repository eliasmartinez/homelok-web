import {CollectionViewer} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, of, Subscription, throwError} from 'rxjs';
import {ActivatedRouteSnapshot, Params, Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {catchError, filter, tap} from 'rxjs/operators';
import {MatTableDataSource} from '@angular/material/table';

export interface GetPageFunctionResponse<T> {
  pageElements: Array<T>;
  nextPageToken: string;
}

export abstract class HomelokListDataSource<T> extends MatTableDataSource<T> {
  private static _SPECIAL_CHAR_REGEX = new RegExp(/\\|\'/g);

  public static MIN_PAGESIZE = 10;
  public static MAX_PAGESIZE = 50;

  private itemsSubject = new BehaviorSubject<T[]>([]);
  private sortChangeSubscription: Subscription;
  private _paginator: MatPaginator;
  private _sort: MatSort;
  private _pageSize: number;
  private nextPageToken: string;
  private currentPageToken: string;
  private filterCriteria: string;
  private cachedPages = new Map<number, Array<T>>();

  public loadingItems = false;
  public currentPage = 0;
  public dateFields: Array<string> = [];

  public data: T[];
  public filterableFields: Array<string>;

  protected route: ActivatedRouteSnapshot;
  protected router: Router;
  protected orderBy: string;

  constructor(pageSize?: number) {
    super();
    if (pageSize) {
      this._pageSize = pageSize;
    } else {
      const fittingPageSize = Math.floor((document.body.offsetHeight - 300) / 40);
      this._pageSize = Math.max(HomelokListDataSource.MIN_PAGESIZE, Math.min(HomelokListDataSource.MAX_PAGESIZE, fittingPageSize));
    }
  }

  public static escapeFilterValue(value: string): string {
    return value.replace(this._SPECIAL_CHAR_REGEX, '\\$&');
  }

  public get paginator(): MatPaginator {
    return this._paginator;
  }

  public set paginator(paginator: MatPaginator) {
    this._paginator = paginator;
    this.setPaginatorDefaults();
  }

  public get sort(): MatSort {
    return this._sort;
  }

  public set sort(sort: MatSort) {
    this._sort = sort;

    if (this.route) {
      if (this.route.queryParams.order || this.orderBy) {
        const [field, dir] = (this.route.queryParams.order || this.orderBy).split(' ');
        if (this.sort.active !== field || this.sort.direction !== dir) {
          this.sort.sort({ id: field, start: dir, disableClear: true });
        }
      } else {
        this.sort.sort({ id: '', start: 'asc', disableClear: false });
      }
    }
  }

  public firstPage(filterCriteria: string, orderBy?: string): Observable<GetPageFunctionResponse<T>> {
    return this.loadPage(filterCriteria, 0, orderBy || this.orderBy);
  }

  public getFilterCriteria(queryParams: Params): string {
    let filterCriteria = '';
    for (const key in queryParams) {
      if (this.filterableFields.indexOf(key) !== -1) {
        if (filterCriteria) {
          filterCriteria += ' && ';
        }
        const value = queryParams[key];
        if (this.dateFields.indexOf(key) !== -1) {
          const end = new Date(Array.isArray(value) ? value[1] : value);
          end.setTime(end.getTime() + 24 * 60 * 60 * 1000);
          filterCriteria += key + ' >= timestamp(\'' + new Date(Array.isArray(value) ? value[0] : value).toISOString() + '\') && '
              + key + ' < timestamp(\'' + end.toISOString() + '\')';
        } else {
          filterCriteria += key + '.contains(\'' + HomelokListDataSource.escapeFilterValue(value) + '\')';
        }
      }
    }
    return filterCriteria;
  }

  // @ts-ignore
  public connect(collectionViewer: CollectionViewer): Observable<T[] | ReadonlyArray<T>> {
    if (this.sort && this.router) {
      this.sortChangeSubscription = this.sort.sortChange.pipe(
          filter((sort: Sort) => !!sort.active)
      ).subscribe((sort: Sort) => {
        this.router.navigate([], {
          queryParams: { order: sort.direction ? sort.active + ' ' + sort.direction : null },
          queryParamsHandling: 'merge'
        }).catch((error: Error) => {
          const [field, dir] = (this.route.queryParams.order || this.orderBy || '').split(' ');
          if (this.sort.direction !== dir || this.sort.active !== field) {
            this.sort.sort({ id: field, start: dir, disableClear: true });
          }
          throw error;
        });
      });
    }
    return this.itemsSubject.asObservable();
  }

  // @ts-ignore
  public disconnect(collectionViewer: CollectionViewer): void {
    if (this.sortChangeSubscription) {
      this.sortChangeSubscription.unsubscribe();
    }
    this.itemsSubject.complete();
  }

  public refreshPage(): Observable<GetPageFunctionResponse<T>> {
    let pageData: T[] = [];
    return this.loadPageFunction(
        this.filterCriteria,
        this.getPageSize(),
        this.currentPageToken,
        this.orderBy
    )
        .pipe(
            tap((response: GetPageFunctionResponse<T>) => {
              pageData = response.pageElements;
              this.nextPageToken = response.nextPageToken;
              this.cachedPages.set(this.currentPage, pageData);
              this.itemsSubject.next(this.cachedPages.get(this.currentPage));
              this.updatePaginator(this.currentPage);
            })
        );
  }

  protected updatePaginator(previousPageIndex: number) {
    if (this.paginator) {
      this.paginator.pageIndex = this.currentPage;
      this.paginator.page.emit({
        previousPageIndex,
        pageIndex: this.currentPage,
        pageSize: this.getPageSize(),
        length: this.cachedPages.get(this.currentPage)?.length
      });
    }
  }

  protected getPageSize(): number {
    return this.paginator?.pageSize || this._pageSize;
  }

  protected abstract loadPageFunction(
      filterCriteria: string,
      pageSize: number,
      pageToken: string,
      orderBy: string): Observable<GetPageFunctionResponse<T>>;

  private setPaginatorDefaults(): void {
    this.paginator.pageSize = this._pageSize;
    this.paginator.hidePageSize = true;
    this.paginator.pageIndex = 0;

    this.paginator.nextPage = () => {
      this.nextPage().subscribe();
    };

    this.paginator.previousPage = () => {
      this.previousPage().subscribe();
    };

    this.paginator.hasNextPage = () => {
      return this.hasNextPage();
    };

    this.paginator.hasPreviousPage = () => {
      return this.hasPreviousPage();
    };
  }

  private nextPage(): Observable<GetPageFunctionResponse<T>> {
    return this.loadPage(this.filterCriteria, this.currentPage + 1, this.orderBy);
  }

  private previousPage(): Observable<GetPageFunctionResponse<T>> {
    return this.loadPage(this.filterCriteria, this.currentPage + 1, this.orderBy);
  }

  private hasNextPage(): boolean {
    return !this.loadingItems && (this.currentPage + 1 < this.cachedPages.size || !!this.nextPageToken);
  }

  private hasPreviousPage(): boolean {
    return !this.loadingItems && this.currentPage > 0;
  }

  private loadPage(filterCriteria: string, page: number, orderBy: string): Observable<GetPageFunctionResponse<T>> {
    let pageData: T[] = [];

    const previousPageIndex = this.currentPage;

    this.loadingItems = true;

    if (page > 0 && this.cachedPages.get(page)?.length > 0) {
      pageData = this.cachedPages.get(page);
      this.itemsSubject.next(this.cachedPages.get(page));
      this.currentPage = page;
      this.loadingItems = false;
      this.updatePaginator(previousPageIndex);
      return of({
        pageElements: pageData,
        nextPageToken: undefined
      });
    } else {
      return this.loadPageFunction(
          filterCriteria,
          this.getPageSize(),
          // Cache is cleaned after first page call is correct
          // So the nextPageToken input MUST be undefined when
          // calling the first page
          page > 0 ? this.nextPageToken : undefined,
          orderBy
      )
          .pipe(
              catchError((error) => {
                this.loadingItems = false;
                this.updatePaginator(this.currentPage);
                return throwError(error);
              }),
              tap(() => {
                if (page === 0) {
                  this.emptyCache();
                }
              }),
              tap((response: GetPageFunctionResponse<T>) => {
                pageData = response.pageElements;
                this.data = pageData;
                this.filterCriteria = filterCriteria;
                this.orderBy = orderBy;
                this.currentPage = page;
                this.currentPageToken = this.nextPageToken;
                this.nextPageToken = response.nextPageToken || undefined;
                this.cachedPages.set(page, pageData);
                this.itemsSubject.next(this.cachedPages.get(page));
                this.loadingItems = false;
                this.updatePaginator(previousPageIndex);
              })
          );
    }
  }

  private emptyCache() {
    this.cachedPages.clear();
    this.currentPageToken = undefined;
    this.nextPageToken = undefined;
  }
}
