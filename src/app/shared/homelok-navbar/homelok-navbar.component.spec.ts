import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomelokNavbarComponent } from './homelok-navbar.component';

describe('HomelokNavbarComponent', () => {
  let component: HomelokNavbarComponent;
  let fixture: ComponentFixture<HomelokNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomelokNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomelokNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
