import {Component} from '@angular/core';
import {SidebarStateService} from './homelok-sidebar-state.service';
import {MenuItem} from '../models/menu-item';
import {onSidebarChange, animateText} from './homelok-sidebar.animations';

@Component({
  selector: 'homelok-sidebar',
  templateUrl: './homelok-sidebar.component.html',
  styleUrls: ['./homelok-sidebar.component.scss'],
  animations: [onSidebarChange, animateText]

})
export class HomelokSidebarComponent {

  public menuItems: MenuItem [];
  public sidebarState = false;
  public linkedText = false;
  public onSidebarChange: boolean;

  constructor(private sidebarStateService: SidebarStateService) {
    this.menuItems = [
      {icon: 'home', route: 'home', title: 'Pods'},
      {icon: 'closed_padlock', route: 'access-points', title: 'Doors'},
      {icon: 'person', route: 'x', title: 'Users'},
      {icon: 'bluetooth', route: 'x', title: 'Devices', endGroup: true},
      {icon: 'log', route: 'x', title: 'Log'},
      {icon: 'notification', route: 'x', title: 'Notification', endGroup: true},
      {icon: 'settings', route: 'x', title: 'Settings'},
    ];
  }

  onSidebarToggle(): void {
    this.sidebarState = !this.sidebarState;

    setTimeout(() => {
      this.linkedText = this.sidebarState;
    }, 200);
    this.sidebarStateService.sidebarState.next(this.sidebarState);
  }
}
