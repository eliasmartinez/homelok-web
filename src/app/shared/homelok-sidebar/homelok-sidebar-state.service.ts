import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidebarStateService {

  public sidebarState: Subject<boolean>;

  constructor() {
    this.sidebarState = new Subject();
  }
}
