import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomelokSidebarComponent } from './homelok-sidebar.component';

describe('HomelokSidebarComponent', () => {
  let component: HomelokSidebarComponent;
  let fixture: ComponentFixture<HomelokSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomelokSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomelokSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
