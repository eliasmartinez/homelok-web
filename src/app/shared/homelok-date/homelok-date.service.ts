import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { DayOfWeek } from '@saltoapis/nebula-types-v1';
import { from, Observable, of } from 'rxjs';
import { map, mapTo, tap } from 'rxjs/operators';
import {DateI18n, MonthIndex, NarrowWeekday} from './homelok-date.class';

export enum DateFormats {
  ShortMonthDay,
  YearMonth,
  YearShortMonth,
  YearLongMonthDay,
}

export enum DateIntervalFormats {
  ShortMonthDay,
}

@Injectable({
  providedIn: 'root'
})
export class HomelokDateService {
  private lang: string;
  private data: { [key: string]: DateI18n } = {};

  private _FIRST_DAY_OF_WEEK = 1;

  constructor(private datePipe: DatePipe) {
  }

  public use(lang: string): Observable<undefined> {
    if (this.data[this.lang]) {
      this.lang = lang;
      return of(undefined);
    }
    return from(import(`../../../i18n/dates/${lang}.json`)).pipe(
      map(res => res.default ?? res),
      tap((c: DateI18n) => {
        this.lang = lang;
        this.data[this.lang] = c;
      }),
      mapTo(undefined)
    );
  }

  public format(date: Date, dateFormat?: string | DateFormats): string {
    if (!this.lang || !this.data[this.lang]) {
      throw new Error('No selected lang');
    }
    let format: string;
    switch (dateFormat) {
      case DateFormats.ShortMonthDay:
        format = this.data[this.lang].dateTimeFormats.availableFormats.MMMd;
        break;
      case DateFormats.YearMonth:
        format = this.data[this.lang].dateTimeFormats.availableFormats.yMMMM;
        break;
      case DateFormats.YearShortMonth:
        format = this.data[this.lang].dateTimeFormats.availableFormats.yMMM;
        break;
      case DateFormats.YearLongMonthDay:
        format = this.data[this.lang].dateFormats.long;
        break;
      default:
        format = dateFormat;
    }
    const weekDays: Array<NarrowWeekday> = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    const localized = (format || this.data[this.lang].dateFormats.medium)
      .replace('EEEEE', '\'' + this.data[this.lang].days.format.narrow[weekDays[date.getDay()]] + '\'')
      .replace('LLLL', '\'' + this.data[this.lang].months['stand-alone'].wide[(date.getMonth() + 1).toString() as MonthIndex] + '\'')
      .replace('LLL', '\'' + this.data[this.lang].months['stand-alone'].abbreviated[(date.getMonth() + 1).toString() as MonthIndex] + '\'')
      .replace('MMMM', '\'' + this.data[this.lang].months.format.wide[(date.getMonth() + 1).toString() as MonthIndex] + '\'')
      .replace('MMM', '\'' + this.data[this.lang].months.format.abbreviated[(date.getMonth() + 1).toString() as MonthIndex] + '\'');
    return this.datePipe.transform(date, localized);
  }

  public formatInterval(date1: Date, date2: Date, format: DateIntervalFormats): string {
    if (!this.lang || !this.data[this.lang]) {
      throw new Error('No selected lang');
    }
    let result: string;
    switch (format) {
      case DateIntervalFormats.ShortMonthDay:
        result = this.data[this.lang].dateTimeFormats.intervalFormats.MMMd.d.replace('d', '\'' + date1.getDate().toString() + '\'');
        break;
    }
    return this.format(date2, result);
  }

  public parse(value: string): Date {
    if (!this.lang || !this.data[this.lang]) {
      throw new Error('No selected lang');
    }
    const months = new Array(12).fill(null).map((e, i) => this.data[this.lang].months.format.abbreviated[(i + 1).toString() as MonthIndex]);
    const format = this.data[this.lang].dateFormats.medium;
    const substitutions: any = {
      y: '([0-9]{1,4})',
      MMM: '(' + months.join('|') + ')',
      d: '([0-9]{1,2})'
    };
    let regex = '';
    let quoted = false;
    for (let i = 0; i < format.length; i++) {
      if (format[i] === '\'') {
        quoted = !quoted;
      } else if (format[i] === '(' || format[i] === ')') {
        regex += '\\' + format[i];
      } else if (format[i].toLowerCase() < 'a' || format[i].toLowerCase() > 'z') {
        regex += format[i];
      } else {
        let token = '';
        while (format[i] && format[i].toLowerCase() >= 'a' && format[i].toLowerCase() <= 'z') {
          token += format[i++];
        }
        i--;
        if (!quoted && substitutions[token]) {
          token = substitutions[token];
        }
        regex += token;
      }
    }
    const result = new RegExp(regex).exec(value);
    if (!result) {
      return new Date(''); // generate "Invalid Date"
    }
    const yIndex = regex.indexOf(substitutions.y);
    const mIndex = regex.indexOf(substitutions.MMM);
    const dIndex = regex.indexOf(substitutions.d);
    const indexes = [yIndex, mIndex, dIndex].sort((a: number, b: number) => a - b);
    const year = parseInt(result[indexes.indexOf(yIndex) + 1], 10);
    const month = months.indexOf(result[indexes.indexOf(mIndex) + 1]);
    const day = parseInt(result[indexes.indexOf(dIndex) + 1], 10);
    return new Date(year, month, day);
  }

  public getFirstDayOfWeek(): number {
    return this._FIRST_DAY_OF_WEEK;
  }

  public localizeDayOfWeek(d: DayOfWeek): string {
    switch (d) {
      case DayOfWeek.MONDAY:
        return this.data[this.lang].days.format.wide.mon;
      case DayOfWeek.TUESDAY:
        return this.data[this.lang].days.format.wide.tue;
      case DayOfWeek.WEDNESDAY:
        return this.data[this.lang].days.format.wide.wed;
      case DayOfWeek.THURSDAY:
        return this.data[this.lang].days.format.wide.thu;
      case DayOfWeek.FRIDAY:
        return this.data[this.lang].days.format.wide.fri;
      case DayOfWeek.SATURDAY:
        return this.data[this.lang].days.format.wide.sat;
      case DayOfWeek.SUNDAY:
        return this.data[this.lang].days.format.wide.sun;
      default:
        return '';
    }
  }
}
