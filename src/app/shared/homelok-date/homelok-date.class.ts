export function dateTrim(data: DateI18n): DateI18n {
  return {
    months: {
      format: {
        abbreviated: data.months.format.abbreviated,
        wide: data.months.format.wide
      },
      'stand-alone': {
        wide: data.months['stand-alone'].wide,
        abbreviated: data.months['stand-alone'].abbreviated
      }
    },
    days: {
      format: {
        narrow: data.days.format.narrow,
        wide: data.days.format.wide
      }
    },
    dateFormats: {
      medium: data.dateFormats.medium,
      long: data.dateFormats.long,
    },
    dateTimeFormats: {
      availableFormats: {
        MMMd: data.dateTimeFormats.availableFormats.MMMd,
        yMMMM: data.dateTimeFormats.availableFormats.yMMMM,
        yMMM: data.dateTimeFormats.availableFormats.yMMM,
      },
      intervalFormats: {
        MMMd: {
          d: data.dateTimeFormats.intervalFormats.MMMd.d,
        }
      }
    }
  };
}

export interface DateI18n {
  'months': {
    'format': {
      'abbreviated': I18nMonthData;
      'wide': I18nMonthData;
    };
    'stand-alone': {
      'wide': I18nMonthData;
      'abbreviated': I18nMonthData;
    };
  };
  'days': {
    'format': {
      'narrow': { [key in NarrowWeekday]: string };
      'wide': { [key in NarrowWeekday]: string };
    };
  };
  'dateFormats': {
    'medium': string;
    'long': string;
  };
  'dateTimeFormats': {
    'availableFormats': {
      'MMMd': string;
      'yMMMM': string;
      'yMMM': string;
    };
    'intervalFormats': {
      'MMMd': {
        'd': string;
      }
    }
  };
}

type I18nMonthData = {
  [key in MonthIndex]: string
};

export type MonthIndex = '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '10' | '11' | '12';

export type NarrowWeekday = 'sun' | 'mon' | 'tue' | 'wed' | 'thu' | 'fri' | 'sat';
