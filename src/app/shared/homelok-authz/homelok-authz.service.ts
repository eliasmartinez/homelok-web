import {Injectable} from '@angular/core';
import {Installation, InstallationService, TestPermissionsRequest, TestPermissionsResponse} from '@saltoapis/nebula-installation-v1';
import {Observable, of, Subject, throwError} from 'rxjs';
import {catchError, mapTo, take, tap} from 'rxjs/operators';
import {HomelokInstallationService} from '../homelok-authnn/homelok-installation.service';

export const HOMELOK_PERMISSIONS = {
  ACCESSPOINT_CONFIGURE: 'nebula.access-point.configure',
  ACCESSPOINT_CREATE: 'nebula.access-point.create',
  ACCESSPOINT_DELETE: 'nebula.access-point.delete',
  ACCESSPOINT_GET: 'nebula.access-point.get',
  ACCESSPOINT_INITIALIZE: 'nebula.access-point.initialize',
  ACCESSPOINT_LIST: 'nebula.access-point.list',
  ACCESSPOINT_TEST: 'nebula.access-point.test',
  ACCESSPOINT_UPDATE: 'nebula.access-point.update',
  ACCESSRIGHT_ACCESSPOINT_CREATE: 'nebula.access-right.access-point.create',
  ACCESSRIGHT_ACCESSPOINT_DELETE: 'nebula.access-right.access-point.delete',
  ACCESSRIGHT_ACCESSPOINT_GET: 'nebula.access-right.access-point.get',
  ACCESSRIGHT_ACCESSPOINT_LIST: 'nebula.access-right.access-point.list',
  ACCESSRIGHT_ACCESSPOINT_UPDATE: 'nebula.access-right.access-point.update',
  ACCESSRIGHT_CREATE: 'nebula.access-right.create',
  ACCESSRIGHT_DELETE: 'nebula.access-right.delete',
  ACCESSRIGHT_GET: 'nebula.access-right.get',
  ACCESSRIGHT_LIST: 'nebula.access-right.list',
  ACCESSRIGHT_UPDATE: 'nebula.access-right.update',
  CALENDAR_CREATE: 'nebula.calendar.create',
  CALENDAR_DELETE: 'nebula.calendar.delete',
  CALENDAR_EVENT_CREATE: 'nebula.calendar.event.create',
  CALENDAR_EVENT_DELETE: 'nebula.calendar.event.delete',
  CALENDAR_EVENT_GET: 'nebula.calendar.event.get',
  CALENDAR_EVENT_LIST: 'nebula.calendar.event.list',
  CALENDAR_EVENT_UPDATE: 'nebula.calendar.event.update',
  CALENDAR_GET: 'nebula.calendar.get',
  CALENDAR_LIST: 'nebula.calendar.list',
  CALENDAR_UPDATE: 'nebula.calendar.update',
  ENCODER_CREATE: 'nebula.encoder.create',
  ENCODER_DELETE: 'nebula.encoder.delete',
  ENCODER_GET: 'nebula.encoder.get',
  ENCODER_LIST: 'nebula.encoder.list',
  ENCODER_UPDATE: 'nebula.encoder.update',
  EVENT_GET: 'nebula.event.get',
  EVENT_LIST: 'nebula.event.list',
  GATEWAY_CREATE: 'nebula.gateway.create',
  GATEWAY_DELETE: 'nebula.gateway.delete',
  GATEWAY_GET: 'nebula.gateway.get',
  GATEWAY_LIST: 'nebula.gateway.list',
  GATEWAY_UPDATE: 'nebula.gateway.update',
  INSTALLATION_CREATE: 'nebula.installation.create',
  INSTALLATION_DELETE: 'nebula.installation.delete',
  INSTALLATION_UPDATE: 'nebula.installation.update',
  POLICY_CREATE: 'nebula.installation.policy.create',
  POLICY_DELETE: 'nebula.installation.policy.delete',
  POLICY_GET: 'nebula.installation.policy.get',
  POLICY_LIST: 'nebula.installation.policy.list',
  POLICY_UPDATE: 'nebula.installation.policy.update',
  REPEATER_CREATE: 'nebula.repeater.create',
  REPEATER_DELETE: 'nebula.repeater.delete',
  REPEATER_GET: 'nebula.repeater.get',
  REPEATER_LIST: 'nebula.repeater.list',
  REPEATER_UPDATE: 'nebula.repeater.update',
  ROLE_GET: 'nebula.iam.role.get',
  ROLE_LIST: 'nebula.iam.role.list',
  USER_ACCESSRIGHT_CREATE: 'nebula.user.access-right.create',
  USER_ACCESSRIGHT_DELETE: 'nebula.user.access-right.delete',
  USER_ACCESSRIGHT_GET: 'nebula.user.access-right.get',
  USER_ACCESSRIGHT_LIST: 'nebula.user.access-right.list',
  USER_ACCESSRIGHT_UPDATE: 'nebula.user.access-right.update',
  USER_CARDKEY_CANCEL: 'nebula.user.card-key.cancel',
  USER_CARDKEY_ENCODE: 'nebula.user.card-key.encode',
  USER_CARDKEY_GET: 'nebula.user.card-key.get',
  USER_CARDKEY_UPDATE: 'nebula.user.card-key.update',
  USER_CREATE: 'nebula.user.create',
  USER_DELETE: 'nebula.user.delete',
  USER_GET: 'nebula.user.get',
  USER_LIST: 'nebula.user.list',
  USER_APPKEY_ASSIGN: 'nebula.user.app-key.assign',
  USER_APPKEY_CANCEL: 'nebula.user.app-key.cancel',
  USER_APPKEY_GET: 'nebula.user.app-key.get',
  USER_APPKEY_UPDATE: 'nebula.user.app-key.update',
  USER_UPDATE: 'nebula.user.update'
};

export const HOMELOK_ROLES: string[] = [
  'iam-roles/admin',
  'iam-roles/user.admin',
  'iam-roles/access-right.admin',
  'iam-roles/device.admin'
];

@Injectable({
  providedIn: 'root'
})
export class HomelokAuthzService {
  private _permissionLoadSource = new Subject<string[]>();
  public permissionChange = this._permissionLoadSource.asObservable();

  private _userPermissionList: string[] = [];

  constructor(
    private homelokInstallationService: HomelokInstallationService,
    private installationService: InstallationService
  ) {
  }

  public loadPermissions(installationParam?: Installation): Observable<boolean> {
    const installation = installationParam ? installationParam.getName() : this.homelokInstallationService.getCurrentInstallationName();
    if (installation === '') {
      return of(true);
    }
    const testPermissionsRequest = new TestPermissionsRequest();
    const permission: string[] = [];
    Object.keys(HOMELOK_PERMISSIONS).forEach((k) => permission.push(HOMELOK_PERMISSIONS[k]));
    testPermissionsRequest.setName(installation);
    testPermissionsRequest.setPermissionsList(permission);
    return this.installationService.testPermissions(testPermissionsRequest).pipe(
      take(1),
      tap((res: TestPermissionsResponse) => {
        this._userPermissionList = res.getPermissionsList();
        this._permissionLoadSource.next(this._userPermissionList || []);
      }),
      mapTo(true),
      catchError((error) => {
        this._userPermissionList = [];
        this._permissionLoadSource.next([]);
        throwError(error);
        return of(false);
      })
    );
  }

  public checkPermissions(permissions: string[]): { [key: string]: boolean } {
    const result = {};
    permissions.forEach((p) => {
      const key = this._getPermissionCodeKey(p);
      result[key] = this._userPermissionList.some((up) => up === p);
    });
    return result;
  }

  private _getPermissionCodeKey(permission: string): string {
    let result = permission;
    Object.keys(HOMELOK_PERMISSIONS).forEach((k) => {
      if (HOMELOK_PERMISSIONS[k] === permission) {
        result = k;
      }
    });
    return result;
  }

  public checkGuardPermissions(permissionCodes: string[]): boolean {
    const permissions = this.checkPermissions(permissionCodes);
    if (Object.values(permissions || {}).includes(false)) {
      throw new Error('Authorization Error');
    } else {
      return true;
    }
  }

}
