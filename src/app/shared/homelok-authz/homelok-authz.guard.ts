import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { HomelokAuthzService } from './homelok-authz.service';

@Injectable({
  providedIn: 'root'
})
export class HomelokAuthzGuard implements CanActivateChild {

  constructor(private authzService: HomelokAuthzService) { }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return route.data ? this.authzService.checkGuardPermissions(route.data.permissions || []) : true;
  }
}
