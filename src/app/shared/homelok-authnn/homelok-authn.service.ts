import {Location} from '@angular/common';
import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {
  AppAuthError,
  AuthorizationNotifier,
  AuthorizationRequest,
  AuthorizationRequestHandler,
  AuthorizationServiceConfiguration,
  BaseTokenRequestHandler,
  FetchRequestor,
  GRANT_TYPE_AUTHORIZATION_CODE,
  GRANT_TYPE_REFRESH_TOKEN,
  StringMap,
  TokenRequest,
  TokenResponse
} from '@openid/appauth';
import * as jwt_decode from 'jwt-decode';
import {forkJoin, from, Observable, of, throwError} from 'rxjs';
import {catchError, filter, finalize, map, mapTo, mergeMap, mergeMapTo, take, tap} from 'rxjs/operators';
import {HomelokInstallationService} from './homelok-installation.service';
import {environment} from '../../../environments/homelok-environment';
import {HomelokAuthzService} from '../homelok-authz/homelok-authz.service';
import {HomelokEnvironment} from '../../../environments/environment.model';

export class NebulaAuthnError extends Error {
  code: number;

  constructor(code: number, message: string) {
    super(message);
    this.code = code;
  }
}

@Injectable({
  providedIn: 'root'
})
export class HomelokAuthnService {

  static readonly ID_TOKEN_HINT_STORAGE_KEY = 'id_token_hint';

  // Beware! the value of this attribute must be in sync with the name of
  // the parameter we are reading in oauth2redirect.html
  // var redirectURI = sessionStorage.getItem('redirect_to');
  static readonly REDIRECT_TO_STORAGE_KEY = 'redirect_to';

  private openIdConnectUrl: string;
  private redirectUri: string;
  private scope = 'openid offline profile email';

  private configuration: AuthorizationServiceConfiguration;
  public notifier: AuthorizationNotifier;

  private tokenResponse: TokenResponse;
  private refreshTokenObservable: Observable<string>;

  private environment: HomelokEnvironment;

  public constructor(
    private authzReqHandler: AuthorizationRequestHandler,
    private tokenHandler: BaseTokenRequestHandler,
    private homelokInstallationService: HomelokInstallationService,
    private homelokAuthzService: HomelokAuthzService,
    private router: Router,
    private location: Location
  ) {
    this.environment = environment;
    this.openIdConnectUrl = 'https://account.saltonebula.com';
    const origin = window.location.origin;
    this.redirectUri = origin + '/oauth2redirect';

    this.notifier = new AuthorizationNotifier();
    this.authzReqHandler.setAuthorizationNotifier(this.notifier);
  }

  public isAuthenticated(): boolean {
    return Boolean(this.tokenResponse);
  }

  public isAuthenticating(): boolean {
    return window.location.hash.indexOf('#code') > -1 ||
      this.hasOIDCFlowFailed();
  }

  private hasOIDCFlowFailed(): boolean {
    return window.location.hash.indexOf('#error') > -1;
  }

  public startAuthnFlow(redirectTo: string, token?: string): Observable<boolean> {
    return this.updateServiceConfiguration().pipe(
      catchError(() => {
        return throwError(new NebulaAuthnError(505, 'Could not get the OIDC server configuration'));
      }),
      tap(() => {
        this.makeAuthorizationRequest(redirectTo, token);
      })
    );
  }

  public getAccessToken(): Observable<string> {
    if (!this.isAuthenticated()) {
      return throwError(new Error('user is not authenticated'));
    }
    if (new Date() >= new Date((this.tokenResponse.issuedAt + this.tokenResponse.expiresIn) * 1000)) {
      return this.refreshToken();
    }
    return of(this.tokenResponse.accessToken);
  }

  public completeAuthnProcess(): Observable<boolean> {
    if (this.hasOIDCFlowFailed()) {
      // If we've tried to sign-in with prompt='none' and the request has been rejected,
      // let's try again but with prompt='consent'
      if (window.localStorage.getItem(HomelokAuthnService.ID_TOKEN_HINT_STORAGE_KEY)) {
        window.localStorage.removeItem(HomelokAuthnService.ID_TOKEN_HINT_STORAGE_KEY);
        return this.startAuthnFlow(window.location.pathname + window.location.search);
      }
      return throwError(new NebulaAuthnError(401, 'OIDC Flow failed'));
    }
    return this.makeTokenRequest().pipe(
      mergeMap(() => this.homelokInstallationService.loadInstallations()),
      mergeMap(() => this.homelokAuthzService.loadPermissions())
    );
  }

  public makeTokenRequest(): Observable<boolean> {
    if (!this.isAuthenticating()) {
      return throwError(new NebulaAuthnError(401, 'Authentication code is missing'));
    }
    if (this.hasOIDCFlowFailed()) {
      return throwError(new NebulaAuthnError(401, 'OIDC Flow failed'));
    }
    return this.updateServiceConfiguration().pipe(
      catchError(() => {
        return throwError(new NebulaAuthnError(505, 'Could not get the OIDC server configuration'));
      }),
      mergeMap(() => forkJoin([
        this.setAuthorizationListenerNotifier(),
        from(this.authzReqHandler.completeAuthorizationRequestIfPossible()).pipe(
          catchError(() => {
            return throwError(new NebulaAuthnError(505, 'An error occurred completing the authorization request'));
          })
        )
      ])),
      mapTo(true)
    );
  }

  public getCurrentUser(): string {
    let result = '';
    if (this.tokenResponse) {
      const payload: any = jwt_decode(this.tokenResponse.idToken);
      result = payload ? payload.sub : '';
    }
    return result;
  }

  private refreshToken(): Observable<string> {
    if (!this.refreshTokenObservable) {
      const request = new TokenRequest({
        client_id: this.environment.oidc.clientId,
        redirect_uri: this.redirectUri,
        grant_type: GRANT_TYPE_REFRESH_TOKEN,
        code: undefined,
        refresh_token: this.tokenResponse.refreshToken,
        extras: undefined
      });
      this.refreshTokenObservable = from(this.tokenHandler.performTokenRequest(this.configuration, request)).pipe(
        catchError((error) => {
          if (error instanceof AppAuthError && error.message === '400') {
            // refresh token expired
            return this.startAuthnFlow(window.location.pathname + window.location.search, undefined).pipe(
              mergeMapTo(throwError(error))
            );
          }
          return throwError(error);
        }),
        tap((tokenResponse) => {
          this.tokenResponse.accessToken = tokenResponse.accessToken;
          this.tokenResponse.refreshToken = tokenResponse.refreshToken;
          this.tokenResponse.issuedAt = tokenResponse.issuedAt;
          this.tokenResponse.expiresIn = tokenResponse.expiresIn;
          this.tokenResponse.tokenType = tokenResponse.tokenType;
          this.tokenResponse.scope = tokenResponse.scope;
        }),
        finalize(() => {
          this.refreshTokenObservable = undefined;
        }),
        map((tokenResponse) => tokenResponse.accessToken)
      );
    }
    return this.refreshTokenObservable;
  }

  private setAuthorizationListenerNotifier(): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      this.notifier.setAuthorizationListener((req, res, error) => {
        if (error) {
          observer.error('An error occurred getting the access token');
        } else if (res) {
          this.getToken(req, res.code).subscribe({
            next: () => {
              this.cleanURL();
              observer.next(true);
              observer.complete();
            },
            error: () => {
              observer.error('Could not get the token');
            }
          }).add(() => {
            sessionStorage.removeItem(HomelokAuthnService.REDIRECT_TO_STORAGE_KEY);
          });
        }
      });
    });
  }

  // Removes all OIDC flow related hash params from the URL.
  private cleanURL() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      take(1)
    ).subscribe(() => {
      if (this.location.path(false) !== this.location.path(true)) {
        this.location.replaceState(this.location.path(false));
      }
    });
  }

  private getToken(request: AuthorizationRequest, code: string): Observable<void> {
    const extras: StringMap = {};
    extras.code_verifier = request.internal.code_verifier;
    // use the code to make the token request.
    const tokenRequest = new TokenRequest({
      client_id: this.environment.oidc.clientId,
      redirect_uri: this.redirectUri,
      grant_type: GRANT_TYPE_AUTHORIZATION_CODE,
      code,
      refresh_token: undefined,
      extras
    });
    return from(this.tokenHandler.performTokenRequest(this.configuration, tokenRequest)).pipe(
      tap((response) => {
        this.tokenResponse = response;
        window.localStorage.setItem(HomelokAuthnService.ID_TOKEN_HINT_STORAGE_KEY, encodeURIComponent(response.idToken));
      }),
      mapTo(void 0)
    );
  }

  private updateServiceConfiguration(): Observable<boolean> {
    if (this.configuration) {
      return of(true);
    }
    return from(AuthorizationServiceConfiguration.fetchFromIssuer(this.openIdConnectUrl, new FetchRequestor())).pipe(
      tap((response) => {
        this.configuration = response;
      }),
      mapTo(true)
    );
  }

  private makeAuthorizationRequest(redirectTo: string, token?: string) {
    const request = new AuthorizationRequest({
      client_id: this.environment.oidc.clientId,
      redirect_uri: this.redirectUri,
      scope: this.scope,
      response_type: AuthorizationRequest.RESPONSE_TYPE_CODE,
      extras: { prompt: 'consent', access_type: 'offline' }
    });

    sessionStorage.setItem(HomelokAuthnService.REDIRECT_TO_STORAGE_KEY, redirectTo);

    if (token) {
      request.extras.token = token;
    } else {
      const idTokenHint = window.localStorage.getItem(HomelokAuthnService.ID_TOKEN_HINT_STORAGE_KEY);
      // if the user has been signed-in previously, try to skip the login screen
      if (idTokenHint) {
        request.extras.prompt = 'none';
        request.extras.id_token_hint = idTokenHint;
      }
    }

    request.setupCodeVerifier().then(() => {
      this.authzReqHandler.performAuthorizationRequest(
        this.configuration,
        request
      );
    });
  }

  public logout(): void {
    const idToken = this.tokenResponse.idToken;
    delete this.tokenResponse;
    window.localStorage.removeItem(HomelokAuthnService.ID_TOKEN_HINT_STORAGE_KEY);
    window.location.replace(this.configuration.endSessionEndpoint +
      '?id_token_hint=' + encodeURIComponent(idToken) +
      '&post_logout_redirect_uri=' + window.location.origin);
  }
}
