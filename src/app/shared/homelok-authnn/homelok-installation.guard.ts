import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import {HomelokInstallationService} from './homelok-installation.service';

@Injectable({
  providedIn: 'root'
})
export class HomelokInstallationGuard implements CanActivate {

  constructor(private nebulaInstallationService: HomelokInstallationService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    if (this.nebulaInstallationService.getCurrentInstallationName()) {
      return of(true);
    } else {
      return this.nebulaInstallationService.installationListLoad.pipe(
        map(installations => installations.find(x => x.getName() === `installations/${route.params.installation}`)),
        tap(installation => {
          if (installation && installation.getName() !== this.nebulaInstallationService.getCurrentInstallationName()) {
            this.nebulaInstallationService.setCurrentInstallation(installation, true);
          }
        }),
        map(installation => Boolean(installation))
      );
    }
  }
}
