import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import {HomelokAuthnService} from './homelok-authn.service';

@Injectable({
  providedIn: 'root'
})
export class HomelokAuthGuard implements CanActivate {

  constructor(private authnService: HomelokAuthnService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.checkUser(state.url.split('#')[0]);
  }

  private checkUser(redirectTo: string): Observable<boolean> {
    const isAuthenticated = this.authnService.isAuthenticated();
    const isAuthenticating = this.authnService.isAuthenticating();
    const shouldLogin = !isAuthenticated && !isAuthenticating;

    if (shouldLogin) {
      const token = new URLSearchParams(window.location.search).get('token');
      this.authnService.startAuthnFlow(redirectTo, token).subscribe();
      return of(false);
    } else if (isAuthenticating) {
      return this.authnService.completeAuthnProcess();
    } else {
      return of(true);
    }
  }
}
