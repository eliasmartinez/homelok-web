import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {GrpcHandler, GrpcInterceptor, GrpcRequest} from '@saltoapis/grpc-web-client';
import {Observable} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {HomelokAuthnService} from './homelok-authn.service';

@Injectable()
export class AuthnGrpcInterceptor implements GrpcInterceptor {

  constructor(private authnService: HomelokAuthnService) { }

  interceptUnary(request: GrpcRequest, next: GrpcHandler): Observable<any> {
    request.metadata = request.metadata || {};
    return this.authnService.getAccessToken().pipe(
      mergeMap((accessToken) => {
        request.metadata.Authorization = `Bearer ${accessToken}`;
        return next.handleUnary(request);
      })
    );
  }

  interceptStream(request: GrpcRequest, next: GrpcHandler): Observable<any> {
    return next.handleStream(request);
  }
}

@Injectable()
export class AuthnHttpInterceptor implements HttpInterceptor {

  constructor(private authnService: HomelokAuthnService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (request.url.indexOf('https://api.saltonebula.com') > -1) {
      return this.authnService.getAccessToken().pipe(
        mergeMap((accessToken) => {
          const newRequest = request.clone({
            setHeaders: {
              Authorization: `Bearer ${accessToken}`
            }
          });
          return next.handle(newRequest);
        })
      );
    } else {
      return next.handle(request);
    }
  }
}
