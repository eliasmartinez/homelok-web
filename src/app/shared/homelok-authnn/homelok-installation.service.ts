import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { DeleteInstallationRequest, Installation, InstallationService, ListInstallationsRequest, ListInstallationsResponse } from '@saltoapis/nebula-installation-v1';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { mapTo, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomelokInstallationService {

  private _installationLoadSource = new Subject<Array<Installation>>();
  public installationListLoad = this._installationLoadSource.asObservable();
  private _currentInstallationSource = new Subject<Installation>();
  public currentInstallation = this._currentInstallationSource.asObservable();

  private _installationList: Installation[] = [];
  private _currentInstallation: Installation;

  constructor(private installationService: InstallationService, private router: Router) {
  }

  public getCurrentInstallation(): Installation {
    return this._currentInstallation;
  }

  public getCurrentInstallationName() {
    return this._currentInstallation ? this._currentInstallation.getName() : '';
  }

  public setCurrentInstallation(value: Installation, preventNavigation = false) {
    this._currentInstallation = value;
    this._currentInstallationSource.next(this._currentInstallation);
    if (!preventNavigation) {
      this.router.navigate(['home']);
    }
  }

  public emptyInstallationList(): Observable<boolean> {
    this._installationList = [];
    this._installationLoadSource.next(this._installationList);
    return of(true);
  }

  public loadInstallations(): Observable<boolean> {
    const listInstallationRequest = new ListInstallationsRequest();
    listInstallationRequest.setPageSize(100);
    return this.installationService.listInstallations(listInstallationRequest).pipe(
      tap((response: ListInstallationsResponse) => {
        this._installationList = response.getInstallationsList();
        if (this._installationList && this._installationList.length > 0) {
          this._currentInstallation = this._currentInstallation ?
            this._installationList.find((i) => i.getName() === this._currentInstallation.getName())
            : this._installationList[0];
          this._currentInstallationSource.next(this._currentInstallation);
        }
        this._installationLoadSource.next(this._installationList);
      }),
      mapTo(true)
    );
  }

  // Dev function to clean testint installations
  public deleteInstallationTests(): Observable<boolean> {
    const deleteInstallationCalls: any[] = [];

    this._installationList.forEach((i) => {
      if (i.getDisplayName().indexOf('Test') > -1) {
        const deleteInstallationRequest = new DeleteInstallationRequest();
        deleteInstallationRequest.setName(i.getName());
        deleteInstallationCalls.push(this.installationService.deleteInstallation(deleteInstallationRequest));
      }
    });

    return forkJoin(deleteInstallationCalls).pipe(mapTo(true));
  }
}
