import {NgModule} from '@angular/core';
import {AuthorizationRequestHandler, BaseTokenRequestHandler, FetchRequestor, RedirectRequestHandler} from '@openid/appauth';
import {GRPC_INTERCEPTORS, GrpcWebClientModule} from '@saltoapis/grpc-web-client';
import {SaltoapisNebulaInstallationV1Module} from '@saltoapis/nebula-installation-v1';
import {SaltoapisNebulaIamV1Module} from '@saltoapis/nebula-iam-v1';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthnGrpcInterceptor, AuthnHttpInterceptor} from './homelok-authn.interceptor';

const authzRequestHandler = new RedirectRequestHandler();
const tokenHandler = new BaseTokenRequestHandler(new FetchRequestor());
@NgModule({
  imports: [
    GrpcWebClientModule,
    SaltoapisNebulaInstallationV1Module,
    SaltoapisNebulaIamV1Module
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthnHttpInterceptor,
      multi: true
    },
    {
      provide: GRPC_INTERCEPTORS,
      useClass: AuthnGrpcInterceptor,
      multi: true
    },
    {
      provide: AuthorizationRequestHandler,
      useValue: authzRequestHandler
    },
    {
      provide: BaseTokenRequestHandler,
      useValue: tokenHandler
    }
  ]
})
export class HomelokAuthnModule { }
