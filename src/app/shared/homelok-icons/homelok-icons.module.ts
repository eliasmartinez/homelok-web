import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { HomelokIconSizeDirective } from './homelok-icon-size.directive';
import {HomelokIconColorDirective} from './homelok-icon-color.directive';

@NgModule({
  exports: [
    MatIconModule,
    HomelokIconSizeDirective,
    HomelokIconColorDirective
  ],
  declarations: [
    HomelokIconSizeDirective,
    HomelokIconColorDirective
  ]
})
export class HomelokIconsModule { }
