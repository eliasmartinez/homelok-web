import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
    selector: '[homelokIconColor]'
})
export class HomelokIconColorDirective implements OnInit {

    @Input() homelokIconColor: string;

    constructor(private el: ElementRef) { }

    ngOnInit() {
        (this.el.nativeElement as HTMLElement).style.fill = this.homelokIconColor;
    }

}
