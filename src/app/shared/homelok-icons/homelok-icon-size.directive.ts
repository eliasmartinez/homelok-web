import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[homelokIconSize]'
})
export class HomelokIconSizeDirective implements OnInit {

  @Input() homelokIconSize: string;

  constructor(private el: ElementRef) { }

  ngOnInit() {
    (this.el.nativeElement as HTMLElement).style.transform = `scale(${parseInt(this.homelokIconSize, 10) / 24})`;
  }

}
