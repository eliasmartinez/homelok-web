import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomelokComponent } from './homelok.component';

describe('HomelokComponent', () => {
  let component: HomelokComponent;
  let fixture: ComponentFixture<HomelokComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomelokComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomelokComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
