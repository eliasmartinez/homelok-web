import {Component, OnDestroy, OnInit} from '@angular/core';
import {onMainContentChange} from './shared/homelok-sidebar/homelok-sidebar.animations';
import {forkJoin, Subscription, throwError} from 'rxjs';
import {SidebarStateService} from './shared/homelok-sidebar/homelok-sidebar-state.service';
import icons from '../assets/icons.svg';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {catchError} from 'rxjs/operators';
import {HomelokDateService} from './shared/homelok-date/homelok-date.service';

@Component({
  selector: 'homelok-root',
  templateUrl: './homelok.component.html',
  styleUrls: ['./homelok.component.scss'],
  animations: [onMainContentChange]
})
export class HomelokComponent implements OnInit, OnDestroy {
  public onSidebarChange: boolean;
  private sidebarStateSubscription: Subscription;

  constructor(
      private sidebarStateService: SidebarStateService,
      private translate: TranslateService,
      private homelokDateService: HomelokDateService,
      public matIconRegistry: MatIconRegistry,
      public domSanitizer: DomSanitizer
  ) {
    // Icons
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl(icons));

    // I18n
    this.configureI18n();

    // Layout
    this.sidebarStateService.sidebarState
        .subscribe(res => this.onSidebarChange = res);
  }

  ngOnDestroy(): void {
    this.sidebarStateSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.sidebarStateSubscription = this.sidebarStateService.sidebarState
        .subscribe(res => this.onSidebarChange = res);
  }

  private configureI18n() {
    this.translate.setDefaultLang('en');
    this.changeLanguage('en');
  }

  private changeLanguage(lang: string) {
    const previousLang = this.translate.currentLang;
    forkJoin([
      this.translate.use(lang),
      this.homelokDateService.use(lang)
    ]).pipe(
        catchError((error: Error) => {
          if (previousLang && lang !== previousLang) {
            this.changeLanguage(previousLang);
          }
          return throwError(error);
        })).subscribe(() => {
      document.documentElement.setAttribute('lang', lang);
    });
  }
}
