import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {HomelokModule} from './app/homelok.module';
import {environment} from './environments/homelok-environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(HomelokModule)
  .catch(err => console.log(err));
