export interface HomelokEnvironment {
  production: boolean;
  enableSW: boolean;
  oidc: { clientId: string };
  googleAPIKey?: string;
}
