import { HomelokEnvironment } from './environment.model';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment: HomelokEnvironment = {
  production: false,
  enableSW: false,
  oidc: { clientId: 'homelok-local' },
};
