import { HomelokEnvironment } from './environment.model';

export const environment: HomelokEnvironment = {
  production: false,
  enableSW: true,
  // TODO -> Waiting for cliend-id to ci environment
  oidc: { clientId: 'homelok-web-local' },
};
