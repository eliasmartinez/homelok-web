import {HomelokEnvironment} from './environment.model';

export const environment: HomelokEnvironment = {
  production: true,
  enableSW: true,
  // TODO -> Waiting for cliend-id to production environment
  oidc: {
    clientId: 'nebula-web'
  },
  googleAPIKey: 'AIzaSyBOCaQ7Me3_1IyM40cJVpHTC7gE6LhuoxY'
};
