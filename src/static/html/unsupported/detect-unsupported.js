(function () {
  var onerror = function () {
    if (!document.body.children[0].innerHTML) {
      window.location.assign('html/unsupported/index.html');
    }
  };
  if (window.addEventListener) {
    window.addEventListener('error', onerror, false);
  } else if (window.attachEvent) {
    window.attachEvent('onerror', onerror);
  } else {
    window.onerror = onerror;
  }
  if (!''.padStart || typeof URLSearchParams === 'undefined') {
    window.location.assign('html/unsupported/index.html');
  }
})();
