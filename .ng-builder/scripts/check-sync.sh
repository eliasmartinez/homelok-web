#!/usr/bin/env bash

set -u
set -e

diff -rq .ng-builder /etc/ng-builder && \
diff <(cat package.json) <(jq ".dependencies += $(jq '.dependencies' .ng-builder/package.json) | .devDependencies += $(jq '.devDependencies' .ng-builder/package.json)" package.json)
exit $?
