const pjson = require(`${__dirname}/../package.json`);
const packages = {};
Object.keys(pjson.dependencies).filter(x => x.match(/^@saltoapis\/.*-v\d+$/)).forEach(x => {
  packages[x] = {
    ignorableDeepImportMatchers: [
      /node_modules\/google-protobuf\//,
    ]
  };
});

module.exports = {
  packages: packages
};
